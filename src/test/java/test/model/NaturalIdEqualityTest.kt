package test.model

import org.hamcrest.MatcherAssert.assertThat
import org.hibernate.Session
import org.junit.jupiter.api.Test
import java.util.HashSet
import kotlin.reflect.KClass


import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue

import org.hamcrest.CoreMatchers.`is` as Is

/**
 * This tests our persistence lifecycle rules where the domain class
 * only implements the naturalId in its equals method.
 *
 * @see <a href="https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier">
 *     Mihalcea's Identity Rule</a> for an explanation of the rules for equality, specifically:
 * "Equals and hashCode must behave consistently across all entity state transitions."
 */
class NaturalIdEqualityTest : AbstractEqualityTest<BookWithNaturalId>() {


    @Test
    fun `entity with only a "naturalId" in equals method will satisfy JPA persistence expectations`() {
        val book = BookWithNaturalId(isbn = "123-456-7890", title = "Wuthering Heights")
        print(book)
        assertEqualityConsistency(BookWithNaturalId::class.java, book)
    }

    @Test
    fun `the copy function, since not overridden in the data class, still benefits from data class behavior`() {
        val book = BookWithNaturalId(isbn = "123-456-7890", title = "Wuthering Heights")
        val copy = book.copy("23-456-7890".reversed())
        print("copied book $copy")
        assertThat(copy.title, Is("Wuthering Heights"))
        val book3 = BookWithNaturalId
        print("pupmonster  $book3.toString()")
    }

    /**
     * Of interest here is
     */
    @Test
    fun `ensure that can fetch what we persisted`() {
        val book = BookWithNaturalId(isbn = "123-456-7890", title = "Wuthering Heights")
        book.id = "newId"
        val entityManager = entityManagerFactory!!.createEntityManager()
        entityManager.transaction.begin()
        entityManager.persist(book)
        entityManager.transaction.commit()
        
        val foundBook = entityManager.find(BookWithNaturalId::class.java, "newId")
        assertThat(foundBook.id, Is("newId"))
    }

}
