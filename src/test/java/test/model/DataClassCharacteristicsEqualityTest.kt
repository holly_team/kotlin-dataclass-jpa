package test.model

import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import java.util.*
import javax.persistence.EntityExistsException
import org.hamcrest.CoreMatchers.`is` as Is

/**
 * This tests our persistence lifecycle rules where the domain class
 * only implements the naturalId in its equals method.
 *
 * @see <a href="https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier">
 *     Mihalcea's Identity Rule</a> for an explanation of the rules for equality.
 *
 * Equals and hashCode must behave consistently across all entity state transitions.
 */
class DataClassCharacteristicsEqualityTest : AbstractEqualityTest<BookWithStandardDataClassCharacteristics>() {

    @Test
    fun `data class should work fine if you do not modify it after persisting it`() {
        val book = BookWithStandardDataClassCharacteristics(_id = UUID.randomUUID().toString(), title = "Wuthering Heights")
        assertEqualityConsistency(BookWithStandardDataClassCharacteristics::class.java, book)
    }


    @Test
    fun `careless use of data classes does not respect standard primary key expectations`() {

        // even with the same id, we have two instances. In memory, that is fine.
        val tuples = HashSet<BookWithStandardDataClassCharacteristics>()
        val id = UUID.randomUUID().toString()
        val book = BookWithStandardDataClassCharacteristics(_id = id, title = "Wuthering Heights")
        tuples.add(book)
        assertThat(tuples.size, Is(1))
        val copy = book.copy()
        copy.title = "Wilhelm Tell"
        tuples.add(copy)
        assertThat(tuples.size, Is(2))

        val entityManager = entityManagerFactory!!.createEntityManager()
        entityManager.transaction.begin()
        entityManager.persist(book)
        entityManager.transaction.commit()
        val foundBook = entityManager.find(BookWithStandardDataClassCharacteristics::class.java, id)
        assertThat(foundBook.id, Is(id))

        // but in the database, we get bitten. @Id in the class is telling the persistence
        // layer that that the id element alone is what counts.
        assertThrows(EntityExistsException::class.java) {
            entityManager.transaction.begin()
            entityManager.persist(copy)
            entityManager.transaction.commit()
        }
    }


    /**
     * Sanity test
     */
    @Test
    fun `ensure that can fetch what we persisted`() {
        val id = UUID.randomUUID().toString()
        val book = BookWithStandardDataClassCharacteristics(_id = id, title = "Wuthering Heights")
        assertEqualityConsistency(BookWithStandardDataClassCharacteristics::class.java, book)

        val entityManager = entityManagerFactory!!.createEntityManager()
        entityManager.transaction.begin()
        entityManager.persist(book)
        entityManager.transaction.commit()

        val foundBook = entityManager.find(BookWithStandardDataClassCharacteristics::class.java, id)
        assertThat(foundBook.id, Is(id))
    }




}
