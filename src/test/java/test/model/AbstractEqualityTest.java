package test.model;


import org.hibernate.Session;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;


/**
 * Equals tests traversing JPA entity lifecycle based on the code by Vlad Mihalcea in this article here:
 *
 * @see <a href="https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier">
 * Mihalcea's Identity Rule</a> for an explanation of the rules for equality, which in turn references:
 * @see <a href=https://docs.oracle.com/javase/10/docs/api/java/lang/Object.html#equals-java.lang.Object-hashCode"> hashCode Rules from JavaDocs
 * <p>
 * These tests were modified from the Original to only include JPA tests and to use JUnit 5.
 */
public abstract class AbstractEqualityTest<T extends Identifiable<? extends Serializable>> extends AbstractEntityTest {

    void assertEqualityConsistency(Class<T> clazz, T entity) {

        Set<T> entities = new HashSet<>();

        entities.add(entity);
        assertTrue(entities.contains(entity));

        doInJPA(entityManager -> {
            entityManager.persist(entity);
            entityManager.flush();
            assertThat("The entity should have been found in the entities set after it was persisted", entities, hasItem(entity));
        });

        assertThat(entities, hasItem(entity));

        doInJPA(entityManager -> {
            T mergedEntity = entityManager.merge(entity);
            System.out.println("_entity = " + mergedEntity);
            assertThat("The entity should have been found in the entities set after it was merged", entities, hasItem(mergedEntity));
        });

        doInJPA(entityManager -> {
            entityManager.unwrap(Session.class).update(entity);
            assertThat("The entity should have been found in the entities set after it was reattached", entities, hasItem(entity));
        });

        doInJPA(entityManager -> {
            T foundEntity = entityManager.find(clazz, entity.getId());
            assertThat("The entity should have been found in the entities set after " +
                    "it was loaded in a subsequent PersistentContext", entities, hasItem(foundEntity));
        });

        doInJPA(entityManager -> {
            T loadedProxyEntity = entityManager.getReference(clazz, entity.getId());
            assertThat("The entity should have been found in the entities set after it was loaded " +
                    "as a Proxy in another PersistenceContext", entities, hasItem(loadedProxyEntity));
        });

        T deletedEntity = doInJPA(entityManager -> {
            T entityInQuestion = entityManager.getReference(clazz, entity.getId());
            entityManager.remove(entityInQuestion);
            return entityInQuestion;
        });

        assertThat("The entity is found the entities set even after it's deleted.", entities, hasItem(deletedEntity));
    }

}
