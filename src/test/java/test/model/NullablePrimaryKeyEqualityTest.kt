package test.model

import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

import org.hamcrest.CoreMatchers.`is` as Is

/**
 * This tests our persistence lifecycle rules where the domain class
 * only implements the naturalId in its equals method.
 *
 * @see <a href="https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier">
 *     Mihalcea's Identity Rule</a> for an explanation of the rules for equality.
 *
 * Equals and hashCode must behave consistently across all entity state transitions.
 */
class NullablePrimaryKeyEqualityTest : AbstractEqualityTest<BookWithGeneratedAndNullablePrimaryKey>() {

    @Test
    fun `entity with a constant id hashCode method -- since @Id is nullable -- fulfils required JPA behavior`() {
        val book = BookWithGeneratedAndNullablePrimaryKey(title = "Wuthering Heights")
        assertEqualityConsistency(BookWithGeneratedAndNullablePrimaryKey::class.java, book)
    }


    @Test
    fun `the copy function, since not overridden in the data class, still benefits from data class behavior`() {
        val book = BookWithGeneratedAndNullablePrimaryKey(title = "Wuthering Heights")
        val copy = book.copy()
        assertThat(copy.title, Is("Wuthering Heights"))
        assertNull(book.id)
        assertThat(book, Is(copy))
    }

    /*
     * Sanity check
     */
    @Test
    fun `ensure that can fetch what we persisted`() {
        // we are not setting the id, Hibernate is doing it for us.
        val book = BookWithGeneratedAndNullablePrimaryKey(title = "Wuthering Heights")
        val entityManager = entityManagerFactory!!.createEntityManager()
        entityManager.transaction.begin()
        entityManager.persist(book)
        entityManager.transaction.commit()
        val foundBook = entityManager.find(BookWithGeneratedAndNullablePrimaryKey::class.java, book.id)
        assertThat(foundBook, Is(notNullValue()))
        assertThat(foundBook.id, Is(book.id))
    }


}
