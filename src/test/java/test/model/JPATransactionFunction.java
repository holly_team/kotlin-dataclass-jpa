package test.model;

import java.util.function.Function;
import javax.persistence.EntityManager;

@FunctionalInterface
public interface JPATransactionFunction<T> extends Function<EntityManager, T> {

}
