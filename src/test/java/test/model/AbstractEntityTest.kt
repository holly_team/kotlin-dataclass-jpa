package test.model


import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.persistence.EntityTransaction
import javax.persistence.Persistence

/**
 * Sets up the entity manager and provides two methods for executing
 * methods in JPA, one that returns void and one that returns a result. Based on code
 * by Vlad Mihalcea, but simplified for this more limited test case scenario.
 *
 * @see [Mihalcea's Identity Rule](https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier) for an explanation of the rules for equality, which in turn references:
 */
abstract class AbstractEntityTest {

    private val LOGGER = LoggerFactory.getLogger(javaClass)

    var entityManagerFactory: EntityManagerFactory? = Persistence.createEntityManagerFactory("test")

    @BeforeEach
    fun init() {
        entityManagerFactory = Persistence.createEntityManagerFactory("test")
    }

    @AfterEach
    fun destroy() {
        if (entityManagerFactory != null) {
            entityManagerFactory!!.close()
        }
    }

    /**
     * Execute a function using JPA
     *
     * @param function the JPA function
     * @return the result
     */
    open fun <T> doInJPA(function: JPATransactionFunction<T>): T {
        val result: T
        var entityManager: EntityManager? = null
        var txn: EntityTransaction? = null
        try {
            entityManager = entityManagerFactory!!.createEntityManager()
            txn = entityManager!!.transaction
            txn!!.begin()
            result = function.apply(entityManager)
            tryToCommit(txn)
        } catch (t: Throwable) {
            if (txn != null && txn.isActive) {
                try {
                    txn.rollback()
                } catch (e: Exception) {
                    LOGGER.error("Rollback failure", e)
                }

            }
            throw t
        } finally {
            entityManager?.close()
        }
        return result
    }

    private fun tryToCommit(txn: EntityTransaction) {
        if (!txn.rollbackOnly) {
            txn.commit()
        } else {
            try {
                txn.rollback()
            } catch (e: Exception) {
                LOGGER.error("Rollback failure", e)
            }

        }
    }

    /**
     * Execute a function using JPA
     *
     * @param function the function to execute
     */
     fun doInJPA(function: JPATransactionVoidFunction) {
        var entityManager: EntityManager? = null
        var txn: EntityTransaction? = null
        try {
            entityManager = entityManagerFactory!!.createEntityManager()
            txn = entityManager!!.transaction
            txn!!.begin()
            function.accept(entityManager)
            tryToCommit(txn)
        } catch (t: Throwable) {
            if (txn != null && txn.isActive) {
                try {
                    txn.rollback()
                } catch (e: Exception) {
                    LOGGER.error("Rollback failure", e)
                }

            }
            throw t
        } finally {
            entityManager?.close()
        }
    }

    companion object {
        init {
            Thread.currentThread().name = "someThreadName"
        }
    }

}
