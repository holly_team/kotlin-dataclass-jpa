package test.model

/**
 *
 * @author S Gertiser, created 2018-09-11.
 */
import org.hamcrest.CoreMatchers.hasItem
import org.hamcrest.CoreMatchers.not
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import kotlin.reflect.full.functions
import org.hamcrest.CoreMatchers.`is` as Is


/**
 * Various demonstrations of standard Kotlin data class behavior
 */
class StandardDataClassDemonstrationTest {

    @Test
    fun `demonstrate how a standard data class uses properties in primary constructor in toString`() {
        val isdn = "978-3-16-148410-0"
        val title = "Wuthering Heights"
        val book = Book(isdn, title)
        assertThat(book.toString(), Is("Book(isdn=$isdn, title=$title)"))
        print(book)
    }

    @Test
    fun `toString no longer sufficiently expressive if the properties are not in primary constructor`() {
        val isdn = "978-3-16-148410-0"
        val title = "Wuthering Heights"
        val book = JpaBook(isdn, title)
        // The equals method is not as expressive as we want.
        Assertions.assertThrows(AssertionError::class.java) {
            assertThat(book.toString(), Is("Book(isdn=$isdn, title=$title)"))
        }
        print(book)
    }

    /**
     * This is an Kotlin data class API exerciser, which illustrates
     * how the interests of an entity class and a Kotlin data class conflict.
     * The copy function, in the case of a JpaBook, does not allow you to copy the title.
     */
    @Test
    fun `copy does not handle properties that are not in the primary constructor`() {
        // Can copy both properties
        val classicalKotlinDataClassBookFunctions = Book::class.functions
        val functionNamesOfClassicalDataClassBook: MutableList<String> = mutableListOf()
        classicalKotlinDataClassBookFunctions.mapTo(functionNamesOfClassicalDataClassBook) { it.toString() }
        assertThat(functionNamesOfClassicalDataClassBook, hasItem("fun test.model.Book.copy(kotlin.String, kotlin.String): test.model.Book"))

        // Cannot copy both properties
        val jpaBookFunctions = JpaBook::class.functions
        val functionNamesOfJpaBook: MutableList<String> = mutableListOf()
        jpaBookFunctions.mapTo(functionNamesOfJpaBook) { it.toString() }
        assertThat(functionNamesOfJpaBook, not(hasItem("fun test.model.Book.copy(kotlin.String, kotlin.String): test.model.Book")))
    }


}