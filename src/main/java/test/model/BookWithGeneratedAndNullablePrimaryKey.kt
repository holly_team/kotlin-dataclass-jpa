package test.model

import au.com.console.kassava.kotlinEquals
import au.com.console.kassava.kotlinToString
import java.util.*
import javax.persistence.*

/**
 * In this case, it is possible for the Id to be null and
 * that requires special handling for JPA,
 * @see #hashCode in this class. For further details, see also:
 * <p>
 * @see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
 */
@Entity
@Access(AccessType.FIELD)
data class BookWithGeneratedAndNullablePrimaryKey(

        val title: String = "undefined" ) : Identifiable<Long?> {

    private var _id: Long? = null

    @Id
    @GeneratedValue
    @Access(AccessType.PROPERTY)
    override fun getId(): Long? {
        return _id
    }

    override fun setId(newId: Long?) {
        _id = newId
    }



    // Here we optionally define properties for equals/toString in a companion object.
    // In this way, Kotlin will generate fewer KProperty classes,
    // and we won't have array creation for every method call.
    companion object {
        private val toStringProperties = arrayOf(BookWithGeneratedAndNullablePrimaryKey::_id, BookWithGeneratedAndNullablePrimaryKey::title)
        // just id for equals
        private val equalsProperties = arrayOf(BookWithGeneratedAndNullablePrimaryKey::_id)
    }

    override fun equals(other: Any?) = kotlinEquals(other = other, properties = equalsProperties)

    override fun toString() = kotlinToString(properties = toStringProperties)

    /**
     *  Id can be null in this class, that is why we set it to a constant (but not a constant in equals)
     */
    override fun hashCode(): Int {
        return Objects.hash(31)
    }


}