package test.model;

import java.io.Serializable;

/**
 * @author S Gertiser, created 2018-09-10.
 */
public interface Identifiable<T extends Serializable> {

    T getId();

    void setId(T id);

}
