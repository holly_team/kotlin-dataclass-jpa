package test.model

import au.com.console.kassava.kotlinEquals
import org.hibernate.annotations.NaturalId
import java.util.*
import javax.persistence.*

/**
 * Hibernate has a nifty @NaturalId annotation, and as such we must
 * override equals and hashCode to use that and only that. If you are not familiar
 * with NaturalId, it is essentially a business key, or what is also known as a
 * friendly id.
 * <p>
 * The isdn field, which is marked as a NaturalId, is never null, and as such we can use it.
 * <p>
 * Note how we are not using id in equals or hashCode, and cannot have it in the constructor
 * for that reason.
 * <p>
 * @author S Gertiser, created 2018-08-31.
 */
@Entity
@EntityListeners(UuidPopulatorPersistenceListener::class)
@Access(AccessType.FIELD)
data class BookWithNaturalId(
                  /** Friendly  or business key */
                  @NaturalId
                  val isbn: String = "undefined",
                  val title: String = "undefined" ) : Identifiable<String> {

    private var _id: String = "undefined"

    @Id
    @Access(AccessType.PROPERTY)
    override fun getId(): String {
        return _id
    }

    override fun setId(id: String) {
        _id = id
    }


//
//    var _id: String? = "undefined"
//
//    override fun setId(id: String?) {
//        _id = id
//    }
//
//
//    @Id
//    var id: String = "undefined"
//        get() { return field }
//        set(value) {id = value}

    // Here we optionally define properties for equals in a companion object.
    // In this way, Kotlin will generate fewer KProperty classes,
    // and we won't have array creation for every method call.
    companion object {
        private val equalsProperties = arrayOf(BookWithNaturalId::isbn)
    }

    override fun equals(other: Any?) = kotlinEquals(other = other, properties = equalsProperties)


    /**
     * isbn is always unique and is the only thing we need in hashCode.
     */
    override fun hashCode(): Int {
        return Objects.hash(isbn)
    }


}