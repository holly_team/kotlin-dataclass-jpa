package test.model

import javax.persistence.Entity
import javax.persistence.Id

/**
 *  For JPA, we only relevant property for equals and hashCode is the isdn, so that
 *  is the only thing that goes into the primary constructor. This comes at the cost
 *  of having an insufficient copy and equals method. The benefit of a Kotlin data class
 *  is compromised.
 */
@Entity
data class JpaBook(@Id val isdn: String = "undefined") {

    var title : String = ""
    // not this unpretty hack to get immutability back
    // since we cannot use this property in the primary constructor
        private set

    constructor(isdn: String, title: String) : this(isdn) {
        this.title = title
    }

}