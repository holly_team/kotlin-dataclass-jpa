package test.model

/**
 * A standard data class with no JPA annotations.
 */
data class Book (val isdn: String, val title: String)