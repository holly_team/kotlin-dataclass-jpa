package test.model

import javax.persistence.*

/**
 * A data class implemented in a basically classical form, i.e. no overrides of equals,
 * or any other function.
 *
 * Here we can modify one aspect of the Id as expressed in the equals, as it is a var.
 * This classical kotlin data class will demonstrate how you can get errors in JPA.
 *
 * @author S Gertiser, created 2018-08-31.
 */
@Entity
data class BookWithStandardDataClassCharacteristics(

        // Both of these mutable properties are used in equality.
        // See tests to see what can happen. Hint: bad things.
        var _id: String? = "undefined",
        var title: String = "undefined" ) : Identifiable<String> {

    @Id
    override fun getId(): String? {
        return _id
    }

    override fun setId(id: String?) {
        _id = id
    }
}