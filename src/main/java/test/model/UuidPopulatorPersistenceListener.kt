package test.model

import javax.persistence.*
import java.util.UUID

/**
 * Populate the Id on persist.
 *
 *
 * @author S Gertiser, created 2018-08-31.
 */
class UuidPopulatorPersistenceListener {

    @PrePersist
    internal fun onPrePersist(o: Any) {
        if (o is Identifiable<*>) {
            if (o.id == null || o.id == "undefined") {
                o.id = UUID.randomUUID().toString()
            }
        } else {
            throw IllegalArgumentException("This function requires a " +
                        "${Identifiable::class.simpleName}")
        }
    }

}